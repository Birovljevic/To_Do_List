package stjepan.to_do_list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class CategoryAdapter extends BaseAdapter{

    private CategoryDBHelper mCategoryDBHelper;
    private List<Category> mCategoryList;

    public CategoryAdapter(Context context) {
        mCategoryDBHelper = CategoryDBHelper.getInstance(context);
        mCategoryList = mCategoryDBHelper.retrieveCategories();
    }

    //daj mi cijelu listu
    public List<Category> getmCategoryList(){
        return this.mCategoryList;
    }
    @Override
    public int getCount() {
        return this.mCategoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mCategoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoryAdapter.CategoryViewHolder holder;
        if(convertView==null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_category, parent, false);
            holder = new CategoryAdapter.CategoryViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (CategoryAdapter.CategoryViewHolder) convertView.getTag();
        }

        Category category = this.mCategoryList.get(position);
        holder.tvCategoryLvCategory.setText(category.getmTitle());

        return convertView;
    }

    static class CategoryViewHolder{
        @BindView(R.id.tvCategoryLvCategory) TextView tvCategoryLvCategory;

        public CategoryViewHolder (View view){
            ButterKnife.bind(this, view);}
    }
    //REMOVE COMMENT: ----------------------------------TU BI MOGLO PUCAT JER NEMA listView-a kojeg bi osvjezio
    public void insertCategory(Category category){
        this.mCategoryDBHelper.insertCategory(category);
        this.refreshData();
    }

    public void removeCategory(int position){
        this.mCategoryDBHelper.removeCategory(this.mCategoryList.get(position));
        this.refreshData();
    }

    public void refreshData(){
        this.mCategoryList = this.mCategoryDBHelper.retrieveCategories();
        this.notifyDataSetChanged();
    }
}
package stjepan.to_do_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class NewCategoryActivity extends Activity {

    @BindView(R.id.lvCategory) ListView lvCategory;
    @BindView(R.id.etNewCategoryTitle) EditText etNewCategoryTitle;
    @BindView(R.id.btnSaveNewCategory) Button btnSaveNewCategory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_category);
        ButterKnife.bind(this);
        CategoryAdapter categoryAdapter = new CategoryAdapter(this);
        this.lvCategory.setAdapter(categoryAdapter);
    }
    @OnClick(R.id.btnSaveNewCategory)
    public void saveNewCategory(){
        String categoryTitle = this.etNewCategoryTitle.getText().toString();
        if (categoryTitle.equals(""))
        {
            Toast.makeText(this, "Enter title!", Toast.LENGTH_SHORT).show();
        }else{
            //TO DO: deklariraj adapter kao globalnu varijablu kako bi se izbjeglo dodatno instanciranje
            Category category = new Category(categoryTitle);
            CategoryAdapter adapter = new CategoryAdapter(this);
            adapter.insertCategory(category);
            returnToMainActivity();
        }
    }
    private void returnToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    @OnItemClick(R.id.lvCategory)
    public void deleteInfo(){
        Toast.makeText(this, "Long click to delete.", Toast.LENGTH_SHORT).show();
    }
    @OnItemLongClick(R.id.lvCategory)
    public boolean deleteCategory(int position){
        CategoryAdapter categoryAdapter = (CategoryAdapter) lvCategory.getAdapter();
        categoryAdapter.removeCategory(position);
        Toast.makeText(this, "Category deleted.", Toast.LENGTH_SHORT).show();
        return false;
    }
}

package stjepan.to_do_list;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class Task {

    //private int mId;
    private String mTitle;
    private String mPriority;
    private String mCategory;

    public Task(){

    }

    public Task(String mTitle, String mPriority, String mCategory) {
        //this.mId = mId;
        this.mTitle = mTitle;
        this.mPriority = mPriority;
        this.mCategory = mCategory;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmPriority() {
        return mPriority;
    }

    public String getmCategory() {
        return mCategory;
    }
}

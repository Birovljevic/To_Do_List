package stjepan.to_do_list;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class CategoryDBHelper extends SQLiteOpenHelper{
    private static CategoryDBHelper mCategoryDBHelper = null;

    private CategoryDBHelper(Context context){
        super(context, Schema.DATABASE_NAME, null, Schema.DATABASE_VERSION);
    }

    public static synchronized CategoryDBHelper getInstance(Context context){
        if (mCategoryDBHelper == null){
            context = context.getApplicationContext();
            mCategoryDBHelper = new CategoryDBHelper(context);
        }
        return mCategoryDBHelper;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_TASK);
        db.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_CATEGORY);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //db.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_TASK);
        db.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_CATEGORY);
        this.onCreate(db);
    }

    public void insertCategory(Category category) {
        ContentValues values = new ContentValues();
        values.put(Schema.CATEGORY_TITLE, category.getmTitle());
        SQLiteDatabase database = getWritableDatabase();
        database.insert(Schema.TABLE_CATEGORY,null,values);
    }

    public void removeCategory(Category category) {
        SQLiteDatabase db = getWritableDatabase();
        String[] whereCondition = new String[]{category.getmTitle()};
        db.delete(Schema.TABLE_CATEGORY, Schema.CATEGORY_TITLE + "=?", whereCondition);
    }

    public List<Category> retrieveCategories() {
        SQLiteDatabase db = getWritableDatabase();
        String[] columns = new String[]{Schema.CATEGORY_TITLE};
        Cursor result = db.query(Schema.TABLE_CATEGORY, columns, null, null, null, null, null);
        return parseTasksFrom(result);
    }
    private List<Category> parseTasksFrom(Cursor result) {
        List<Category> categoryList = new ArrayList<>();
        if(result.moveToFirst()){
            do{
                String title = result.getString(result.getColumnIndex(Schema.CATEGORY_TITLE));
                Category category = new Category(title);
                categoryList.add(category);
            }while(result.moveToNext());
            result.close();
        }
        return categoryList;
    }
}

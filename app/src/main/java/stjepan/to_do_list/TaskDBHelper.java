package stjepan.to_do_list;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.xml.validation.Schema;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class TaskDBHelper extends SQLiteOpenHelper{

    private static TaskDBHelper mTaskDBHelper = null;

    private TaskDBHelper(Context context){
        super(context, stjepan.to_do_list.Schema.DATABASE_NAME, null, stjepan.to_do_list.Schema.DATABASE_VERSION);
    }

    public static synchronized TaskDBHelper getInstance(Context context){
        if (mTaskDBHelper == null){
            context = context.getApplicationContext();
            mTaskDBHelper = new TaskDBHelper(context);
        }
        return mTaskDBHelper;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_TASK);
        db.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_CATEGORY);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_TASK);
        //db.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_CATEGORY);
        this.onCreate(db);
    }

    public void insertTask(Task task) {
        ContentValues values = new ContentValues();
        values.put(stjepan.to_do_list.Schema.TASK_TITLE, task.getmTitle());
        values.put(stjepan.to_do_list.Schema.TASK_PRIORITY, task.getmPriority());
        values.put(stjepan.to_do_list.Schema.TASK_CATEGORY, task.getmCategory());
        SQLiteDatabase database = getWritableDatabase();
        database.insert(stjepan.to_do_list.Schema.TABLE_TASK,null,values);
    }

    public void removeTask(Task task) {
        SQLiteDatabase db = getWritableDatabase();
        String[] whereCondition = new String[]{task.getmTitle()};
        db.delete(stjepan.to_do_list.Schema.TABLE_TASK, stjepan.to_do_list.Schema.TASK_TITLE + "=?", whereCondition);
    }


    public List<Task> retrieveTasks() {

        SQLiteDatabase db = getWritableDatabase();
        String[] columns = new String[]{stjepan.to_do_list.Schema.TASK_TITLE, stjepan.to_do_list.Schema.TASK_PRIORITY, stjepan.to_do_list.Schema.TASK_CATEGORY};

        Cursor result = db.query(stjepan.to_do_list.Schema.TABLE_TASK, columns, null, null, null, null, null);
        //Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + stjepan.to_do_list.Schema.TABLE_TASK + "'", null);
        //if (cursor.getCount() == 1){
        //    cursor = db.query(stjepan.to_do_list.Schema.TABLE_TASK, columns, null, null, null, null, null);
            return parseTasksFrom(result);
    }


    private List<Task> parseTasksFrom(Cursor result) {
        List<Task> taskList = new ArrayList<>();
        if(result.moveToFirst()){
            do{
                String title = result.getString(result.getColumnIndex(stjepan.to_do_list.Schema.TASK_TITLE));
                String priority = result.getString(result.getColumnIndex(stjepan.to_do_list.Schema.TASK_PRIORITY));
                String category = result.getString(result.getColumnIndex(stjepan.to_do_list.Schema.TASK_CATEGORY));
                Task task = new Task(title, priority, category);
                taskList.add(task);
            }while(result.moveToNext());
            result.close();
        }
        return taskList;
    }
}

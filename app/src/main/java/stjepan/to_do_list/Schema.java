package stjepan.to_do_list;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class Schema {

    // database info:
    public static String DATABASE_NAME = "to_do_list";
    public static int DATABASE_VERSION = 1;

    // database tables:
    public static String TABLE_TASK = "task";
    public static String TABLE_CATEGORY = "category";

    // table column names:
    // table task:
    public static String TASK_ID = "_id";
    public static String TASK_TITLE = "title";
    public static String TASK_PRIORITY = "priority";
    public static String TASK_CATEGORY = "category";

    //table category:
    public static String CATEGORY_ID = "_id";
    public static String CATEGORY_TITLE = "title";

}

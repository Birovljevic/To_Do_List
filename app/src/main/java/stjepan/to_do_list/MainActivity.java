package stjepan.to_do_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

    @BindView(R.id.lvTasks) ListView lvTasks;
    @BindView(R.id.btnNewTask) Button btnNewTask;
    @BindView(R.id.btnNewCategory) Button btnNewCategory;

    private Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        TaskAdapter taskAdapter = new TaskAdapter(this);
        this.lvTasks.setAdapter(taskAdapter);
    }
    @OnClick(R.id.btnNewTask)
    public void clickNewTask(){
        intent = new Intent(this, NewTaskActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.btnNewCategory)
    public void clickNewCategory(){
        intent = new Intent(this, NewCategoryActivity.class);
        startActivity(intent);
    }
    @OnItemClick(R.id.lvTasks)
    public void deleteInfo(){
        Toast.makeText(this, "Long click to delete.", Toast.LENGTH_SHORT).show();
    }
    @OnItemLongClick(R.id.lvTasks)
    public boolean deleteTask(int position){
        TaskAdapter taskAdapter = (TaskAdapter) lvTasks.getAdapter();
        taskAdapter.removeTask(position);
        Toast.makeText(this, "Task deleted.", Toast.LENGTH_SHORT).show();
        return false;
    }
}

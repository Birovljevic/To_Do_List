package stjepan.to_do_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewTaskActivity extends Activity {

    @BindView(R.id.etNewTaskTitle) EditText edNewTaskTitle;
    @BindView(R.id.spnNewTaskPriority) Spinner spnNewTaskPriority;
    @BindView(R.id.spnNewTaskCategory) Spinner spnNewTaskCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        ButterKnife.bind(this);
        initializePrioritySpinner();
        initializeCategorySpinner();
    }

    private void initializePrioritySpinner() {
        //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.priority_array,android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.priority_array,R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spnNewTaskPriority.setAdapter(adapter);
    }

    private void initializeCategorySpinner() {

        CategoryAdapter categoryAdapter = new CategoryAdapter(this);
        List<String> spinnerArray = new ArrayList<>();
        List<Category> categoryList = categoryAdapter.getmCategoryList();
        //ako postoje kategorije u bazi:
        if (categoryList.size() > 0){
            for (Category category:categoryList) {
                spinnerArray.add(category.getmTitle());
            }
            //ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item, spinnerArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            this.spnNewTaskCategory.setAdapter(adapter);
        }
    }

    @OnClick(R.id.btnSaveNewTask)
    public void saveNewTask(){
        String title = this.edNewTaskTitle.getText().toString();
        String priority = this.spnNewTaskPriority.getSelectedItem().toString();
        String category;

        if (title.equals("")){
            Toast.makeText(this, "Enter title!", Toast.LENGTH_SHORT).show();
        }else{
            //jel spinner mora u try-catch jer vraca null ukoliko kategorija ne postoji?
            try{
                category = spnNewTaskCategory.getSelectedItem().toString();
            }catch (Exception e){
                e.printStackTrace();
                category = "Other";
            }

            Task task = new Task(title, priority, category);
            TaskAdapter taskAdapter = new TaskAdapter(this);
            taskAdapter.insertTask(task);

            returnToMainActivity();
        }
    }
    private void returnToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

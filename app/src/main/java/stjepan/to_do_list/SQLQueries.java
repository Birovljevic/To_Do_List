package stjepan.to_do_list;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class SQLQueries {
    static class CREATE_TABLES{
        static String CREATE_TABLE_TASK = "CREATE TABLE " + Schema.TABLE_TASK + " ("
                + Schema.TASK_ID + " INTEGER PRIMARY KEY, "
                + Schema.TASK_TITLE + " TEXT UNIQUE NOT NULL, "
                + Schema.TASK_PRIORITY + " TEXT, "
                + Schema.TASK_CATEGORY + " TEXT);";

        static String CREATE_TABLE_CATEGORY = "CREATE TABLE " + Schema.TABLE_CATEGORY + " ( "
                + Schema.CATEGORY_ID + " INTEGER PRIMARY KEY, "
                + Schema.CATEGORY_TITLE + " TEXT UNIQUE NOT NULL);";
    }

    static class DROP_TABLES{
        static String DROP_TABLE_TASK = "DROP TABLE IF EXISTS " + Schema.TABLE_TASK;
        static String DROP_TABLE_CATEGORY = "DROP TABLE IF EXISTS " + Schema.TABLE_CATEGORY;
    }

}

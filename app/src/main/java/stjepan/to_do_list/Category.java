package stjepan.to_do_list;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class Category {

    //private int mId;
    private String mTitle;

    public Category(){

    }

    public Category(String mTitle) {
        //this.mId = mId;
        this.mTitle = mTitle;
    }

    public String getmTitle() {
        return mTitle;
    }
}

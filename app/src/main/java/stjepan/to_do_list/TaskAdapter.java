package stjepan.to_do_list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Stjepan on 8.11.2017..
 */

public class TaskAdapter extends BaseAdapter {

    private TaskDBHelper mTaskDBHelper;
    private List<Task> mTaskList;

    public TaskAdapter(Context context) {
        mTaskDBHelper = TaskDBHelper.getInstance(context);
        mTaskList = mTaskDBHelper.retrieveTasks();
    }

    //TO DO:
    //public List<Category> getAllCategories(){
    // return this.CategoryList}
    @Override
    public int getCount() {
        return this.mTaskList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mTaskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TaskViewHolder holder;
        if(convertView==null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_task, parent, false);
            holder = new TaskViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (TaskViewHolder) convertView.getTag();
        }

        Task task = this.mTaskList.get(position);
        holder.tvTaskLvTask.setText(task.getmTitle());
        holder.tvCategoryLvTask.setText(task.getmCategory());
        holder.tvPriorityLvTask.setText(task.getmPriority());

        return convertView;
    }

    static class TaskViewHolder{
        @BindView(R.id.tvTaskLvTask) TextView tvTaskLvTask;
        @BindView(R.id.tvCategoryLvTask) TextView tvCategoryLvTask;
        @BindView(R.id.tvPriorityLvTask) TextView tvPriorityLvTask;

        public TaskViewHolder (View view){
            ButterKnife.bind(this, view);}
    }
    //REMOVE COMMENT: ----------------------------------TU BI MOGLO PUCAT JER NEMA listView-a kojeg bi osvjezio
    public void insertTask(Task task){
        this.mTaskDBHelper.insertTask(task);
        this.refreshData();
    }

    public void removeTask(int position){
        this.mTaskDBHelper.removeTask(this.mTaskList.get(position));
        this.refreshData();
    }

    /*
    public String getUserPoints(int position) {
        User user = this.mUserList.get(position);
        int points = this.mUsersDBHelper.getPointsInfo(user);
        return user.getName() + ": " + points;
    }
     */

    public void refreshData(){
        this.mTaskList = this.mTaskDBHelper.retrieveTasks();
        this.notifyDataSetChanged();
    }
}
